﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

	public Text time;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float timer = Time.time;
		time.text = Mathf.Floor(timer / 60).ToString("00") + ':' + (timer % 60).ToString("00");
	}
}
