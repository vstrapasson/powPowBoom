﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class ShotController : MonoBehaviour {

    abstract protected float getSpeed();
    abstract protected float GetRecoil();
    abstract protected AudioClip GetAudio();

    public string side = "right";

    void Start()
    {

        Rigidbody rigidbody = this.GetComponent<Rigidbody>();
        AudioSource.PlayClipAtPoint(GetAudio(), transform.position);

        Vector3 vector = rigidbody.transform.eulerAngles;
        vector.z += Random.Range(GetRecoil(), GetRecoil() * -1);
        rigidbody.transform.eulerAngles = vector;

        if (side == "right")
        {
            rigidbody.velocity = transform.right * getSpeed();
        }
        else
        {
            rigidbody.velocity = (transform.right * getSpeed()) * -1;
        }

        Destroy(gameObject, 0.5f);
    }


}
