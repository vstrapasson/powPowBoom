﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoldierShotController : ShotController
{

    public float speed;
    public float recoil;
    public AudioClip sound;

    protected override float getSpeed()
    {
        return speed;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Sniper")
        {
            Animator animator = collision.gameObject.GetComponent<Animator>();
            animator.SetBool("dead", true);
            killPlayer(collision.gameObject);

        }
        if (gameObject.name == collision.gameObject.name)
        {
            return;
        }
        Destroy(gameObject);
    }

    protected void killPlayer(GameObject go)
    {
        PlayerController player = go.GetComponent<PlayerController>();

        GameObject spawn = GameObject.Find("SpawnSniper");
        Spawn spawnScript = spawn.GetComponent<Spawn>();

        Destroy(go);

        Text text = GameObject.Find("SoldierScoreCount").GetComponent<Text>();
        text.text = (System.Int32.Parse(text.text) + 1).ToString();

        Instantiate(spawnScript.prefab, spawn.transform.position, spawn.transform.rotation);
    }

    protected override float GetRecoil()
    {
        return recoil;
    }

    protected override AudioClip GetAudio()
    {
        return sound;
    }
}