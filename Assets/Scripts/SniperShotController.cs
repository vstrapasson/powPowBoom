﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SniperShotController : ShotController {

    public float speed;
    public float recoil;
    public AudioClip sound;

    protected override float getSpeed()
    {
        return speed;
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Soldier")
        {
            Animator animator = collision.gameObject.GetComponent<Animator>();
            animator.SetBool("dead", true);
            killPlayer(collision.gameObject);

        }
        if (gameObject.name == collision.gameObject.name)
        {
            return;
        }
        Destroy(gameObject);
    }

    protected void killPlayer(GameObject go)
    {
        PlayerController player = go.GetComponent<PlayerController>();

        GameObject spawn = GameObject.Find("SpawnSoldier");
        Spawn spawnScript = spawn.GetComponent<Spawn>();

        Destroy(go);

        Text text = GameObject.Find("SniperScoreCount").GetComponent<Text>();
        text.text = (System.Int32.Parse(text.text) + 1).ToString();

        Instantiate(spawnScript.prefab, spawn.transform.position, spawn.transform.rotation);
    }

    protected override float GetRecoil()
    {
        return recoil;
    }

    protected override AudioClip GetAudio()
    {
        return sound;
    }
}
