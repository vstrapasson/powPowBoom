﻿using UnityEngine;
using System.Collections;

public class SniperController : PlayerController {

    public GameObject shot;
    public float fireRate = 0F;
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    private string jumpInput = "JumpSniper";
    private string horizontalInput = "HorizontalSniper";
    private string fireInput = "FireSniper";
    private ShotController shotController;

    protected override string GetJumpInput()
    {
        return jumpInput;
    }

    protected override string GetHorizontalInput()
    {
        return horizontalInput;
    }

    protected override string GetFireInput()
    {
        return fireInput;
    }

    protected override IEnumerator Fire()
    {
        lastShot = Time.time;

        yield return new WaitForSeconds(0.45F);
        Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
    }

    protected override Animator GetAnimator()
    {
        if (!animator)
        {
            animator = GetComponent<Animator>();
        }

        return animator;
    }

    protected override ShotController GetShotController()
    {
        if (!shotController)
        {
            shotController = GetShot().GetComponent<ShotController>();
        }

        return shotController;
    }

    protected override GameObject GetShot()
    {
        return shot;
    }

    protected override float GetSpeed()
    {
        return speed;
    }

    protected override float GetJumpSpeed()
    {
        return jumpSpeed;
    }

    protected override float GetGravity()
    {
        return gravity;
    }

    protected override float GetFireRate()
    {
        return fireRate;
    }

    protected override Transform GetShotSpawn()
    {
        return shotSpawn;
    }
}
