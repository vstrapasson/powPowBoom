﻿using UnityEngine;
using System.Collections;

public abstract class PlayerController : MonoBehaviour {

    private Vector3 moveDirection = Vector3.zero;
    protected Animator animator;
    protected string lastSide = "right";
    private ShotController shotMover;
    public Transform shotSpawn;
    protected float lastShot = 0.0f;

    abstract protected string GetJumpInput();
    abstract protected string GetHorizontalInput();
    abstract protected string GetFireInput();
    abstract protected IEnumerator Fire();
    abstract protected Animator GetAnimator();
    abstract protected ShotController GetShotController();
    abstract protected GameObject GetShot();
    abstract protected float GetSpeed();
    abstract protected float GetJumpSpeed();
    abstract protected float GetGravity();
    abstract protected float GetFireRate();
    abstract protected Transform GetShotSpawn();

    void Start()
    {
        animator = GetComponent<Animator>();
        shotMover = GetShot().GetComponent<ShotController>();
    }

    void Update()
    {
        TriggerAnimations();
        ChangePlayerMoveSide();

        CharacterController controller = GetComponent<CharacterController>();

        if (controller.isGrounded)
        {
            moveDirection = CalculeMoveDirection();
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= GetSpeed();
            if (Input.GetButton(GetJumpInput()))
                moveDirection.y = GetJumpSpeed();

        }
        moveDirection.y -= GetGravity() * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);

        if (Input.GetButton(GetFireInput()) && Time.time > GetFireRate() + lastShot)
        {
            StartCoroutine(Fire());
        }
    }

    protected void TriggerAnimations()
    {
        Move(Input.GetAxis(GetHorizontalInput()) != 0);
        Fire(Input.GetButton(GetFireInput()));
        Jump(Input.GetButton(GetJumpInput()));
    }

    protected void ChangePlayerMoveSide()
    {
        float horizontal = Input.GetAxis(GetHorizontalInput());
        if ((horizontal > 0 && GetLastSide() == "left") || (horizontal < 0 && GetLastSide() == "right"))
        {

            transform.localScale = new Vector3(
            transform.localScale.x * -1,
            transform.localScale.y,
            transform.localScale.z);

            lastSide = horizontal > 0 ? "right" : "left";
            SetShotMoverSide(transform.localScale.x);
        }
    }

    protected Vector3 CalculeMoveDirection()
    {
        int side = lastSide == "right" ? 1 : -1;

        Vector3 direction = new Vector3(Input.GetAxis(GetHorizontalInput()) * side, 0, 0);

        return direction;
    }

    protected void Move(bool flag)
    {
        GetAnimator().SetBool("move", flag);
    }

    protected void Fire(bool flag)
    {
        GetAnimator().SetBool("fire", flag);
    }

    protected void Jump(bool flag)
    {
        GetAnimator().SetBool("up", flag);
    }

    protected void Crouch(bool flag)
    {
        GetAnimator().SetBool("down", flag);
    }

    protected string GetLastSide()
    {
        return this.lastSide;
    }

    protected void SetShotMoverSide(float side)
    {
        GetShotController().side = side > 0 ? "right" : "left";
    }
}
